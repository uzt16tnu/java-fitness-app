package fitnessapp;

import java.time.LocalDate;
import java.time.Period;

public class Validators {
    public static boolean validateText(String text, int length){
        return text.length() <= length;
    }
    
    public static boolean validatePasswords(String pass1, String pass2){
        if (pass1.equals(pass2))
            return true;
        else
            return false;
    }
    
    public static boolean validateEmail(String email){
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }
    
    public static boolean validateNumber(String number, int min, int max){
        if (number.matches("-?\\d+(\\.\\d+)?")){
            if (Double.parseDouble(number) > min && Double.parseDouble(number) < max){
                return true;
            }
        }
        return false;
    }
    
    public static boolean validateDateOfBirth(LocalDate date){
        return Period.between(date, LocalDate.now()).getYears() > 12;
    }
    
    public static boolean validateWeightDate(LocalDate date){
        return Period.between(date, LocalDate.now()).getDays() < 7 && 
               Period.between(date, LocalDate.now()).getDays() >= 0;
    }
}
