package fitnessapp;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;

public class ExerciseLog {
    private int calories;
    private final LocalDate date;
    private final Account user;
    
    public ExerciseLog(int calories, LocalDate date, Account user){
        this.calories = calories;
        this.date = date;
        this.user = user;
    }
    
    public int getCalories(){
        return this.calories;
    }
    
    public void setCalories(int calories){
        this.calories = calories;
    }
    
    //Add the local instance to the database 
    public void create(){
        String sql = "INSERT INTO exerciselog(calories_burnt, user_id, date) VALUES(?,?,?)";
 
        try (Connection conn = DatabaseManager.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setDouble(1, this.calories);
            pstmt.setInt(2, this.user.getId());
            pstmt.setDate(3, Date.valueOf(this.date));
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    //Update the calories of the equivalent database record 
    public void updateCalories() {
        String sql = "UPDATE exerciselog SET calories_burnt = calories_burnt + ? WHERE date = ? AND user_id = ? ";
 
        try (Connection conn = DatabaseManager.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
 
            pstmt.setDouble(1, this.calories);
            pstmt.setDate(2, Date.valueOf(this.date));
            pstmt.setInt(3, this.user.getId());
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
