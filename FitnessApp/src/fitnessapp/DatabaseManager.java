package fitnessapp;

import java.security.*;
import java.sql.*;
import java.time.LocalDate;

public class DatabaseManager {
    
    public static Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:java.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    
    //Password hash function
    public static String hashPassword(String password) {
        String hashed = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            hashed = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            System.out.println("Hashing Failed");
        }
        return hashed;
    }
    
    //Check if the user with login credentials is in the database
    public static boolean checkUser(String email, String password){
        String hashed = hashPassword(password);
        String sql = "SELECT COUNT(*) FROM user WHERE email = ? AND password = ?";
        
        try (Connection conn = connect();
             PreparedStatement pstmt  = conn.prepareStatement(sql)) {
            pstmt.setString(1, email);
            pstmt.setString(2, hashed);
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            if (rs.getInt("COUNT(*)") == 1)
                return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
    
    //Check if a weight log is in the database for the given date and current user
    public static boolean checkWeightLog(LocalDate date){
        String sql = "SELECT COUNT(*) FROM weightlog WHERE date = ? AND user_id = ?";
        
        try (Connection conn = connect();
             PreparedStatement pstmt  = conn.prepareStatement(sql)) {
            pstmt.setDate(1, Date.valueOf(date));
            pstmt.setInt(2, FitnessApp.currentUser.getId());
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            if (rs.getInt("COUNT(*)") == 1)
                return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
    
    //Check if a food log is in the database for the given date and current user
    public static boolean checkFoodLog(LocalDate date){
        String sql = "SELECT COUNT(*) FROM foodlog WHERE date = ? AND user_id = ?";
        
        try (Connection conn = connect();
             PreparedStatement pstmt  = conn.prepareStatement(sql)) {
            pstmt.setDate(1, Date.valueOf(date));
            pstmt.setInt(2, FitnessApp.currentUser.getId());
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            if (rs.getInt("COUNT(*)") == 1)
                return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
    
    //Check if a exercise log is in the database for the given date and current user
    public static boolean checkExerciseLog(LocalDate date){
        String sql = "SELECT COUNT(*) FROM exerciselog WHERE date = ? AND user_id = ?";
        
        try (Connection conn = connect();
             PreparedStatement pstmt  = conn.prepareStatement(sql)) {
            pstmt.setDate(1, Date.valueOf(date));
            pstmt.setInt(2, FitnessApp.currentUser.getId());
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            if (rs.getInt("COUNT(*)") == 1)
                return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
    
    //Login user using their unique email
    public static void logInUser(String email){
        String sql = "SELECT * FROM user WHERE email = ?";
        
        try (Connection conn = connect();
             PreparedStatement pstmt  = conn.prepareStatement(sql)) {
            pstmt.setString(1, email);
            ResultSet rs = pstmt.executeQuery();
            
            rs.next();
            Account.Gender gender = null;
            switch (rs.getString("gender")){
                case "M":
                    gender = Account.Gender.Male;
                    break;
                case "F":
                    gender = Account.Gender.Female;
                    break;
            }
            Account.ActivityLevel level = null;
            switch (rs.getString("gender")){
                case "L":
                    level = Account.ActivityLevel.Low;
                    break;
                case "M":
                    level = Account.ActivityLevel.Medium;
                    break;
                case "H":
                    level = Account.ActivityLevel.High;
                    break;
            }
            Account user = new Account( rs.getString("first_name"),
                                        rs.getString("last_name"),
                                        rs.getDate("date_of_birth").toLocalDate(),
                                        gender,
                                        rs.getString("email"),
                                        rs.getString("password"),
                                        level,
                                        rs.getDouble("weight"),
                                        rs.getInt("height"));
            //Set the current user
            FitnessApp.currentUser = user;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //Get calories consumed for the logged in used for a specific date
    public static int getCaloriesConsumed(LocalDate date){
        String sql = "SELECT calories_consumed FROM foodlog WHERE date = ? AND user_id = ?";
        
        try (Connection conn = connect();
             PreparedStatement pstmt  = conn.prepareStatement(sql)) {
            pstmt.setDate(1, Date.valueOf(date));
            pstmt.setInt(2, FitnessApp.currentUser.getId());
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            return rs.getInt("calories_consumed");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }
    
    //Get calories burned for the logged in used for a specific date
    public static int getCaloriesBurned(LocalDate date){
        String sql = "SELECT calories_burnt FROM exerciselog WHERE date = ? AND user_id = ?";
        
        try (Connection conn = connect();
             PreparedStatement pstmt  = conn.prepareStatement(sql)) {
            pstmt.setDate(1, Date.valueOf(date));
            pstmt.setInt(2, FitnessApp.currentUser.getId());
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            return rs.getInt("calories_burnt");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }
}
