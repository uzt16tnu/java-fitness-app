
package fitnessapp;

import static fitnessapp.Validators.*;
import javafx.fxml.*;
import java.io.IOException;
import java.net.URL;
import java.time.*;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class HomeViewController implements Initializable {
    @FXML
    private Label goalCal;
    
    @FXML
    private Label foodCal;
    
    @FXML
    private Label exerCal;
    
    @FXML
    private Label remainingCal;
    
    @FXML
    private Label bmi;
    
    @FXML
    private Label currentWeight;

    @FXML
    private TextField newWeightField;
    
    @FXML
    private DatePicker newWeightDateField;
    
    @FXML
    private TextField caloriesConsumedField;
    
    @FXML
    private TextField caloriesBurnedField;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        currentWeight.setText(Double.toString(FitnessApp.currentUser.getWeight()));
        bmi.setText(Double.toString(FitnessApp.currentUser.calcBMI()));
        int bmr = FitnessApp.currentUser.calcBMR();
        goalCal.setText(Integer.toString(bmr));
        int consumed = DatabaseManager.getCaloriesConsumed(LocalDate.now());
        foodCal.setText(Integer.toString(consumed));
        int burned = DatabaseManager.getCaloriesBurned(LocalDate.now());
        exerCal.setText(Integer.toString(burned));
        remainingCal.setText(Integer.toString(bmr - consumed + burned));
    }    
    
    public void handleCheckInWeightBtn(ActionEvent event) throws IOException{
        if(validateNumber(newWeightField.getText(), 40, 500) &&
           validateWeightDate(newWeightDateField.getValue())){
            WeightLog log = new WeightLog(Double.parseDouble(newWeightField.getText()),
                                          newWeightDateField.getValue(),
                                          FitnessApp.currentUser);
            if(DatabaseManager.checkWeightLog(newWeightDateField.getValue())){
                //Update weight log
                log.updateWeight();
            }
            else{
                //Create weight log
                log.create();
            }
            //Update user weight
                FitnessApp.currentUser.setWeight(Double.parseDouble(newWeightField.getText()));
                FitnessApp.currentUser.updateWeight();
            
            //Switch to home view
            Parent pane = FXMLLoader.load(getClass().getResource("HomeView.fxml"));
            Scene registerScene = new Scene(pane);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(registerScene);
            stage.show();
        }
        else{
            Alert errorAlert = new Alert(AlertType.ERROR);
            errorAlert.setHeaderText("Input not valid");
            errorAlert.setContentText("Please enter valid data");
            errorAlert.showAndWait();
        }
    }
    
    public void handleLogFoodBtn(ActionEvent event) throws IOException{
        if(validateNumber(caloriesConsumedField.getText(), 0, 2000)){
            FoodLog log = new FoodLog(Integer.parseInt(caloriesConsumedField.getText()),
                                      LocalDate.now(),
                                      FitnessApp.currentUser);
            if(DatabaseManager.checkFoodLog(LocalDate.now())){
                //Update weight log
                log.updateCalories();
            }
            else{
                //Create weight log
                log.create();
            }
            
            //Switch to home view
            Parent pane = FXMLLoader.load(getClass().getResource("HomeView.fxml"));
            Scene registerScene = new Scene(pane);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(registerScene);
            stage.show();
        }
        else{
            Alert errorAlert = new Alert(AlertType.ERROR);
            errorAlert.setHeaderText("Input not valid");
            errorAlert.setContentText("Please enter valid data");
            errorAlert.showAndWait();
        }
    }
    
    public void handleLogExerciseBtn(ActionEvent event) throws IOException{
        if(validateNumber(caloriesBurnedField.getText(), 0, 2000)){
            ExerciseLog log = new ExerciseLog(Integer.parseInt(caloriesBurnedField.getText()),
                                      LocalDate.now(), 
                                      FitnessApp.currentUser);
            if(DatabaseManager.checkExerciseLog(LocalDate.now())){
                //Update weight log
                log.updateCalories();
            }
            else{
                //Create weight log
                log.create();
            }
            
            //Switch to home view
            Parent pane = FXMLLoader.load(getClass().getResource("HomeView.fxml"));
            Scene registerScene = new Scene(pane);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(registerScene);
            stage.show();
        }
        else{
            Alert errorAlert = new Alert(AlertType.ERROR);
            errorAlert.setHeaderText("Input not valid");
            errorAlert.setContentText("Please enter valid data");
            errorAlert.showAndWait();
        }
    }
    
    public void handleSignOutBtn(ActionEvent event) throws IOException{
        //Unasign current user
        FitnessApp.currentUser = null;
        //Switch to login view
            Parent pane = FXMLLoader.load(getClass().getResource("LogInView.fxml"));
            Scene registerScene = new Scene(pane);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(registerScene);
            stage.show();
    }
}
