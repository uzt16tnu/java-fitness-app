package fitnessapp;

import static fitnessapp.Validators.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class RegisterController implements Initializable {
    
    @FXML
    private TextField firstNameField;
    
    @FXML
    private TextField lastNameField;
    
    @FXML
    private TextField emailField;
    
    @FXML
    private PasswordField passwordField;
    
    @FXML
    private PasswordField passwordCheckField;
    
    @FXML
    private DatePicker dobField;
    
    @FXML
    private ComboBox<Account.Gender> genderField;
    
    @FXML
    private ComboBox<Account.ActivityLevel> activityField;
    
    @FXML
    private TextField weightField;
    
    @FXML
    private TextField heightField;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        genderField.getItems().addAll(Account.Gender.values());
        activityField.getItems().addAll(Account.ActivityLevel.values());
    }    
 
    public void handleRegisterBtn(ActionEvent event) throws IOException{
        if (validateText(firstNameField.getText(), 30) &&
            validateText(lastNameField.getText(), 30) &&
            validateEmail(emailField.getText()) &&
            validatePasswords(passwordField.getText(),passwordCheckField.getText()) &&
            validateDateOfBirth(dobField.getValue()) &&
            validateNumber(weightField.getText(), 40, 500) &&
            validateNumber(heightField.getText(), 100, 300)){
            
            //Make new local user instance
            Account user = new Account( firstNameField.getText(),
                                        lastNameField.getText(),
                                        dobField.getValue(),
                                        genderField.getValue(),
                                        emailField.getText(),
                                        DatabaseManager.hashPassword(passwordField.getText()),
                                        activityField.getValue(),
                                        Double.parseDouble(weightField.getText()),
                                        Integer.parseInt(heightField.getText()));
            //Set current user 
            FitnessApp.currentUser = user;
            //Commit to database
            FitnessApp.currentUser.create();
            //Switch to home view
            Parent pane = FXMLLoader.load(getClass().getResource("HomeView.fxml"));
            Scene registerScene = new Scene(pane);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(registerScene);
            stage.show();
        }
        else {
            Alert errorAlert = new Alert(AlertType.ERROR);
            errorAlert.setHeaderText("Input not valid");
            errorAlert.setContentText("Please enter valid data");
            errorAlert.showAndWait();
        }
    }
}
