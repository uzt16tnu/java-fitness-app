package fitnessapp;

import java.sql.*;
import java.time.*;

public class Account {
    
    public static enum Gender { Male, Female };
    public static enum ActivityLevel { High, Medium, Low };

    private final String firstName;
    private final String lastName;
    private final LocalDate DoB;
    private final Gender gender;
    private final String email;
    private final String password;
    private final ActivityLevel activityLevel;
    private double weight;
    private final int height;
    
    public Account(String firstName,
                   String lastName,
                   LocalDate DoB,
                   Gender gender,
                   String email,
                   String password,
                   ActivityLevel activityLevel,
                   double weight,
                   int height){
        
        this.firstName = firstName;
        this.lastName = lastName;
        this.DoB = DoB;
        this.gender = gender;
        this.email = email;
        this.password = password;
        this.activityLevel = activityLevel;
        this.weight = weight;
        this.height = height;
    }
    
    public double getWeight(){
        return Math.round(this.weight * 100.0)/ 100.0;
    }
    
    public void setWeight(double weight){
        this.weight = weight;
    }
    
    public double calcBMI(){
        return Math.round((weight/((height/100.0) * (height/100.0))) * 100.0) / 100.0;
    }
    
    public int calcBMR(){
        int bmr = (int)((10 * this.weight) + (6.25 * this.height) - (5 * this.calcAge()));
        bmr += this.gender == Account.Gender.Male ? 5 : -161;
        //Adjust BMR dependant on activity level
        switch (this.activityLevel) {
            case Low:
                bmr = (int)(bmr * 1.2);
                break;
            case Medium:
                bmr = (int)(bmr * 1.375);
                break;
            case High:
                bmr = (int)(bmr * 1.55);
                break;
        }
        return bmr;
    }
    
    //Add the local instance to the database 
    public void create(){
        String sql = "INSERT INTO user(first_name, last_name, email, date_of_birth,"
                + "weight, height, activity_level, gender, password) VALUES(?,?,?,?,?,?,?,?,?)";
 
        try (Connection conn = DatabaseManager.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, this.firstName);
            pstmt.setString(2, this.lastName);
            pstmt.setString(3, this.email);
            pstmt.setDate(4, Date.valueOf(this.DoB));
            pstmt.setDouble(5, this.weight);
            pstmt.setInt(6, this.height);
            pstmt.setString(7, this.activityLevel.toString().substring(0, 1));
            pstmt.setString(8, this.gender.toString().substring(0, 1));
            pstmt.setString(9, this.password);
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //Update the weight of the equivalent database record 
    public void updateWeight() {
        String sql = "UPDATE user SET weight = ? WHERE email = ?;";
 
        try (Connection conn = DatabaseManager.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
 
            pstmt.setDouble(1, this.weight);
            pstmt.setString(2, this.email);
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //Get id of equivalent database record
    public int getId() {
        String sql = "SELECT id FROM user WHERE email = ?;";
        int id;
        try (Connection conn = DatabaseManager.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, this.email);
            ResultSet rs= pstmt.executeQuery();
            rs.next();
            id = rs.getInt("id");
        } catch (SQLException e) {
            id = 0;
            System.out.println(e.getMessage());
        }
        return id;
    }
    
    //Calulate Age
    private int calcAge(){
        return Period.between(this.DoB, LocalDate.now()).getYears();
    }
    
    
}


