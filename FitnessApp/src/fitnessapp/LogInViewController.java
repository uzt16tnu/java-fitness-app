package fitnessapp;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.stage.Stage;


public class LogInViewController implements Initializable {
    
    @FXML
    private TextField logInEmailField;
    
    @FXML
    private PasswordField logInPasswordField;
    
    public void handleLogInBtn(ActionEvent event) throws IOException{
        if (DatabaseManager.checkUser(logInEmailField.getText(), logInPasswordField.getText())){
            //Log in user
            DatabaseManager.logInUser(logInEmailField.getText());
            //Switch to hame view
            Parent pane = FXMLLoader.load(getClass().getResource("HomeView.fxml"));
            Scene registerScene = new Scene(pane);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(registerScene);
            stage.show();
        }
        else{
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setHeaderText("User not found");
            errorAlert.setContentText("Please enter valid log in");
            errorAlert.showAndWait();
        }
        
    }
    
    public void handleSignUpBtn(ActionEvent event) throws IOException{
        //Change View
        Parent pane = FXMLLoader.load(getClass().getResource("RegisterView.fxml"));
        Scene registerScene = new Scene(pane);
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(registerScene);
        stage.show();
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
}