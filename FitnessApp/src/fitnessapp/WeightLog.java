package fitnessapp;

import java.sql.*;
import java.time.LocalDate;

public class WeightLog {
    private final double weight;
    private final LocalDate date;
    private final Account user;
    
    public WeightLog(double weight, LocalDate date, Account user){
        this.weight = weight;
        this.date = date;
        this.user = user;
    }
    
    //Add the local instance to the database 
    public void create(){
        String sql = "INSERT INTO weightlog(weight, user_id, date) VALUES(?,?,?)";
 
        try (Connection conn = DatabaseManager.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setDouble(1, this.weight);
            pstmt.setInt(2, this.user.getId());
            pstmt.setDate(3, Date.valueOf(this.date));
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    //Update the weight of the equivalent database record
    public void updateWeight() {
        String sql = "UPDATE weightlog SET weight = ? WHERE date = ? AND user_id = ?";
 
        try (Connection conn = DatabaseManager.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
 
            pstmt.setDouble(1, this.weight);
            pstmt.setDate(2, Date.valueOf(this.date));
            pstmt.setInt(3, this.user.getId());
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
